import { render, screen } from "@testing-library/react";

import App from "./App";

test("ui-home: renders page title", () => {
  render(<App />);
  const linkElement = screen.getByText(/Learning Testing/i);
  expect(linkElement).toBeInTheDocument();
});

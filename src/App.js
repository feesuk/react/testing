import React, { useState } from "react";
import { emailChacker } from "./Helper";

export default function App() {
  const [mail, setMail] = useState("");

  return (
    <div>
      <h1>Learning Testing</h1>
      <hr />
      <div>Input Your Email :</div>
      <input
        placeholder="input your email"
        value={mail}
        onChange={({ target: { value } }) => setMail(value)}
        type="email"
      />
      {mail && !emailChacker(mail) && <div>Email Format is Wrong</div>}
    </div>
  );
}

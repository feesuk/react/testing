import { emailChacker, addition } from "./index";

describe("function: emailChecker", () => {
  let testCase = "jdoe@example.com";
  it(`should return 'true' when invoking the function with ${testCase}`, () => {
    const result = emailChacker(testCase);
    expect(result).toBe(true);
  });
});

describe("function: emailChecker", () => {
  let testCase = "jdoe@example";
  it(`should return 'false' when invoking the function with ${testCase}`, () => {
    const result = emailChacker(testCase);
    expect(result).toBe(false);
  });
});

describe("function: emailChecker", () => {
  let testCase = "jdoe.com";
  it(`should return 'false' when invoking the function with ${testCase}`, () => {
    const result = emailChacker(testCase);
    expect(result).toBe(false);
  });
});

describe("function: addition", () => {
  let x = 1;
  let y = 2;
  it(`should return 'addition of ${x} and ${y}' when invoking the function`, () => {
    const result = addition(1, 2);
    expect(result).toBe(3);
  });
});
